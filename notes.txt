POSTGRES tutorial:
https://www.youtube.com/watch?v=qw--VYLpxG4&list=WL&index=1&ab_channel=freeCodeCamp.org

datagrip: best GUI client (made by jetbrains)

****************************************BASICS********************************************

change user to postgres and start psql:
su postgres
psql

to quit postgres:
\q

to list DB:
\list or \l

to create a DB:
CREATE DATABASE <DB_NAME>;

to connect to DB
\connect or \c <DB_NAME>;

To show tables in DB:
\dt

To create a table (e.g. person):
CREATE TABLE person(id int, first_name VARCHAR(50), last_name VARCHAR(50), genre VARCHAR(6), date_of_birth TIMESTAMP);

To describe a table:
\d <TABLE_NAME>

to see current connections:
SELECT * FROM pg_stat_activity;

to delete DB:
DROP DATABASE <DB_NAME>;

to delete table:
DROP TABLE <TABLE_NAME>;

Null is null in SQL 

use single quotes for strings

RECORDS is a line in a table (an instance)

****************************************CONTRAINTS********************************************
Assuming connected to a DB and having all privileges

Creating table with constraints:

CREATE TABLE constrained_person(id BIGSERIAL NOT NULL PRIMARY KEY, first_name VARCHAR(50) NOT NULL, last_name VARCHAR(50) NOT NULL, genre VARCHAR(50) NOT NULL, date_of_birth DATE NOT NULL);

- BIGSERIAL -> signed int automatically increment itself
- NOT NULL -> error if value is empty


****************************************USERS********************************************

to show users:
\du

to create a user:
CREATE USER <USERNAME>;
ALTER USER <USERNAME> WITH PASSWORD <PASSWORD>;

to grant privileges to this user:
GRANT ALL PRIVILEGES ON DATABASE <DB_NAME> TO <USERNAME>;
GRANT ALL PRIVILEGES ON ALL TABLES in SCHEMA public TO <USERNAME>;

To create new DB with a owner:
CREATE DATABASE <DB_NAME> WITH OWNER = <USERNAME>;

to create log as other user;
first do that: https://stackoverflow.com/questions/18664074/getting-error-peer-authentication-failed-for-user-postgres-when-trying-to-ge

psql -U <USER_NAME>

****************************************CHEATSHEETS******************************************
data type: https://www.postgresql.org/docs/9.5/datatype.html

generate mock data: https://www.mockaroo.com/ 

to run SQL script:
\i /home/jc/Documents/repo/postgres-tuto/MOCK_DATA.sql

to change the view (expended view): \x

****************************************SQL********************************************
***********
Inserting:
INSERT INTO constrained_person (first_name, last_name, genre, date_of_birth) VALUES ('JC', 'Dansereau','Male','1990-11-03'); 

No need to specify the ID, since it auto increment itself

***********
Deleting:
to delete all the table
DELETE FROM <TABLE_NAME>

to delete just id 1001:
DELETE from <TABLE_NAME> where id=1001;

***********
Updating:

Update every row with name=JC:
UPDATE person SET first_name = 'JC';

update for 1 id: 
UPDATE person SET first_name = 'JC' WHERE id=1005;

***********
Selecting:
All:
SELECT * FROM person;

Only rows:
select id, first_name from person;

***********
Order by (sorting)  (works for number, string)
SELECT * from person ORDER BY gender;

Same as this (asc by default):
SELECT * from person ORDER BY gender ASC;

ordered by descending:
SELECT * from person ORDER BY gender DESC;

***********
Filtering:

-------
Distinct (will only output 1 of each, here only 1 instance of each gender) (aka remove duplicates)
SELECT DISTINCT gender from person;

-------
Where clause (only output the data):
SELECT * from person where gender = 'Female';

multiple where:
SELECT * from person where (gender = 'Female' OR gender = 'Male');

with combinaison:
SELECT * from person where gender = 'Female' ORDER BY first_name;

-------
Arithmetic operator:
SELECT * FROM person where id < 4;

-------
Limiting number of results:
SELECT * from person LIMIT 10;

getting result 5 to 15:
SELECT * from person OFFSET 5 LIMIT 10;

-------
IN Keyword: 

SELECT * from person where gender = 'Male' OR gender =  'female' or gender = 'Non-binary';
is the same as 

SELECT * from person where gender IN ('Male', 'Female', 'Non-binary');

-------
BETWEEN Keyword to select data in a range:

SELECT * from person where date_of_birth BETWEEN DATE '1990-01-01' AND '1990-12-31';

-------
LIKE operator (wildcard):

last name finish by 'ver':
SELECT * FROM person WHERE last_name LIKE '%ver'

% -> any caracter

any char + 'ver' + any char
SELECT * FROM person WHERE last_name LIKE '%ver%'

2 chars + 'ver':
SELECT * FROM person WHERE last_name LIKE '__ver';

-------
ILIKE operator (wildcard IGNORE CASE (will return all that match not case sensitive)):
SELECT * from person where last_name ILIKE 'p%';

-------
GROUP BY:
SELECT gender, COUNT(*) FROM person GROUP BY gender; 
that will output the number of instance per gender (e.g. female (64))

-------
HAVING Keyword:
works with GROUP BY

SELECT gender, COUNT(*) FROM person GROUP BY gender HAVING COUNT(*) >=110; 

filter and output only when >= 100, 
WHERE is bit like WHERE

-------
USING Keyword:
when the foreign key and the primary key of the other table have the same name.
USING (car) to say using from table car;

****************************************Functions********************************************
With table car:
\d car 

id
make
model
price 

-------
MAX, MIN and average: 
SELECT MAX(price) FROM car;
SELECT MIN(price) FROM car;
SELECT AVG(price) FROM car;

-------
ROUND:
SELECT ROUND(AVG(price)) FROM car;

-------
Min price per make 

SELECT make, MIN(price) FROM car GROUP BY make;

-------
SUM all the prices
SELECT SUM(price) FROM car;


SUM by make:
SELECT SUM(price) FROM car;

-------
ROUND 

SELECT id,make,model,price, ROUND(0.9*price,2) from car;

2 -> 2 decimal

-------
ALIAS 

SELECT id,make,model,price, ROUND(0.9*price,2) AS ten_percent_off from car;

as -> alias

****************************************Handling Null********************************************
COALESCE :
SELECT COALESCE(gender, 'No gender available') from person;

will output all gender, but if gender is null will replace by No gender available

-------
NULLIF

SELECT 10/NULLIF(0, 0);
supposed to be safe division 

****************************************TIMESTAMP and DATE********************************************
Get the current date:
SELECT NOW();
SELECT NOW()::TIME;
SELECT NOW()::DATE;

-------
INTERVAL:

SELECT NOW() - INTERVAL '1 YEAR';

-------
EXTRACT

SELECT EXTRACT(YEAR FROM NOW());

2020

-------
AGE :
SELECT AGE(NOW(), '1990-11-03');

30 years 8 mons 23 days 07:55:34.328879

-------


****************************************PRIMARY KEY********************************************
PRIMARY KEY when create a table 

PRIMARY KEY cannot be duplicate!
This is a contraint

Dropping the PRIMARY KEY contraint :
ALTER TABLE person DROP CONTRAINT "person_pkey";

-------
Adding a PRIMARY KEY:

ALTER TABLE person ADD PRIMARY KEY (id);

need to have no duplicates!

-------
UNIQUE CONTRAINT

allows to have unique value for a column

to add a unique constraint:

ALTER TABLE person ADD CONSTRAINT unique_gender UNIQUE (gender);

to see constraint: \d person 

****************************************FOREIGN KEY AND RELATIONSHIP********************************************
With the tables person and car, we want 1 car person max.

Foreign Key (FK) is a column that references a primary key (PK) from another table.
See screenshot

In Car table we have the primary key (id), which is a foreign key in person table (called car_id)

drop table person, car;
run the foreign_key_test.sql script 

Assign car_id to a person:
UPDATE person SET car_id=1 where first_name='JC';

if I try to assign a non-valid key, will fail.

**********
INNER JOINS 
Combine 2 tables, only records that fits the ON condition

expended display on:
\x
ex. Joining table person and car together using the foreign key as the joiner. (WILL NOT SAVE, JUST DISPLAY)
SELECT * from person JOIN car ON person.car_id = car.id;

**********
LEFT JOINS
Combine 2 tables including the column that has no match with ON condition

SELECT * from person LEFT JOIN car ON car.id = person.car_id;

**********
To add a foreign key if is missing:
UPDATE person SET car_id=3 WHERE id=3;


**********
DELETING RECORDS with foreign key

cannot delete a record if its ID (primary key) is referenced in another table.
e.g. 

cannot 
DELETE FROM car where id=3;
if car_id=3 is used in table person.

need to do:
delete the person aka:
DELETE FROM person where car_id=3;

OR

UPDATE person SET car_id=NULL where car_id=3;

****************************************SPECIAL DATA TYPE********************************************
BIGSERIAL: 
int that increment by itself 

in table person we have a BIGSERIAL 

to see data about it:
SELECT * from person_id_seq;

***********
UUID:

check available extensions for postgresql
select * from pg_available_extensions;

craete the UUID extension:
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

we now have some functions to deal with UUID: 

invoke the function:
SELECT uuid_generate_v4();


-----
UUID as primary key:

look in the file uuid_ex.sql, but looks like:
id UUID NOT NULL PRIMARY KEY

and call it with: uuid_generate_v4()

UPDATE person2 SET car_uuid_foreign='6ccfdccd-2b9b-4726-9ae4-af608517c598' where first_name='JC';

****************************************MISC********************************************
save table to .csv:
COPY persons TO 'C:\tmp\persons_db.csv' DELIMITER ',' CSV HEADER;


****************************************ADVANCED RELATIONSHIP********************************************
One-to-one (1:1)
A relationship is one-to-one if and only if one record from table A is related to a maximum of one record in table B.

To establish a one-to-one relationship, the primary key of table B (with no orphan record) must be the secondary key of table A (with orphan records).

For example:

CREATE TABLE Gov(
    GID number(6) PRIMARY KEY, 
    Name varchar2(25), 
    Address varchar2(30), 
    TermBegin date,
    TermEnd date
); 

CREATE TABLE State(
    SID number(3) PRIMARY KEY,
    StateName varchar2(15),
    Population number(10),
    SGID Number(4) REFERENCES Gov(GID), 
    CONSTRAINT GOV_SDID UNIQUE (SGID)
);

INSERT INTO gov(GID, Name, Address, TermBegin) 
values(110, 'Bob', '123 Any St', '1-Jan-2009');

INSERT INTO STATE values(111, 'Virginia', 2000000, 110);
One-to-many (1:M)
A relationship is one-to-many if and only if one record from table A is related to one or more records in table B. However, one record in table B cannot be related to more than one record in table A.

To establish a one-to-many relationship, the primary key of table A (the "one" table) must be the secondary key of table B (the "many" table).

For example:

CREATE TABLE Vendor(
    VendorNumber number(4) PRIMARY KEY,
    Name varchar2(20),
    Address varchar2(20),
    City varchar2(15),
    Street varchar2(2),
    ZipCode varchar2(10),
    Contact varchar2(16),
    PhoneNumber varchar2(12),
    Status varchar2(8),
    StampDate date
);

CREATE TABLE Inventory(
    Item varchar2(6) PRIMARY KEY,
    Description varchar2(30),
    CurrentQuantity number(4) NOT NULL,
    VendorNumber number(2) REFERENCES Vendor(VendorNumber),
    ReorderQuantity number(3) NOT NULL
);
Many-to-many (M:M)
A relationship is many-to-many if and only if one record from table A is related to one or more records in table B and vice-versa.

To establish a many-to-many relationship, create a third table called "ClassStudentRelation" which will have the primary keys of both table A and table B.

CREATE TABLE Class(
    ClassID varchar2(10) PRIMARY KEY, 
    Title varchar2(30),
    Instructor varchar2(30), 
    Day varchar2(15), 
    Time varchar2(10)
);

CREATE TABLE Student(
    StudentID varchar2(15) PRIMARY KEY, 
    Name varchar2(35),
    Major varchar2(35), 
    ClassYear varchar2(10), 
    Status varchar2(10)
);  

CREATE TABLE ClassStudentRelation(
    StudentID varchar2(15) NOT NULL,
    ClassID varchar2(14) NOT NULL,
    FOREIGN KEY (StudentID) REFERENCES Student(StudentID), 
    FOREIGN KEY (ClassID) REFERENCES Class(ClassID),
    UNIQUE (StudentID, ClassID)
);


---------------------------------------
DROP ALL DB flask_restapi connections:

SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'flask_restapi'
  AND pid <> pg_backend_pid();



--------------------------------------
HACKERRANK notes:
Distinc: to have no duplicate outputted:
(also can make modulo and operator here)

select DISTINCT city from station where id%2=0;


******COUNT

SELECT COUNT ( DISTINCT cust_code ) AS "Number of employees" FROM orders;

find all the distinc cust_code and sum them.

COUNT difference:
select COUNT(city) - COUNT(DISTINCT city) from station;



********LENGTH
to output the length of a string:

select city, LENGTH(city) from station;


Order by length:
select city, LENGTH(city) from station ORDER BY LENGTH(city);



********MAX and MIN
select the name and the lenght of city where it is the longest

select city, LENGTH(city) from station where LENGTH(city)=(select MAX(LENGTH(city)) from station);


*****WHERE WITH ORDER BY
SELECT LoginID FROM Employee WHERE VacationHours = 8 ORDER BY HireDate;

*****LIMIT
only outputs a certain amount at max

SELECT contact_id FROM contacts WHERE website = 'TechOnTheNet.com' ORDER BY contact_id DESC LIMIT 5;



********************************SUBSTR,LEFT AND RIGHT
To get substring.
MyStr = "this is a test"
LEFT(Mystr, 3)   will output the 3 left char of Mystr  -> thi
RIGHT(Mystr, 4) will ouput the 4 right char MyStr      -> test

SUBSTR will output substring with start index.
SUBSTR(STR, start, lenght)
SUBSTR(MyStr, 5,2) -> is


******************************REGEX
SELECT * FROM USERS WHERE LIKE '^[aeiouAEIOU];

********************************ORDER BY MULTIPLE CONDITION
Order by last 3 char of name.
If they are equal, order by ID ASC. :
SELECT NAME FROM STUDENTS WHERE MARKS > 75 ORDER BY RIGHT(NAME,3), ID ASC;



****************************Sort by alphabetical name

SELECT NAME FROM EMPLOYEE ORDER BY NAME ASC;


*****************************
Write a query that prints a list of employee names (i.e.: the name attribute) for employees in Employee having a salary greater than 2000 per month who have been employees for less than  months. Sort your result by ascending employee_id.

SELECT NAME FROM EMPLOYEE WHERE (SALARY > 2000 AND MONTHS < 10) ORDER BY EMPLOYEE_ID;




*************************** COUNT
TO COUNT the number of outputs
SELECT COUNT(POPULATION) FROM CITY WHERE POPULATION > 100000;

output: 6
