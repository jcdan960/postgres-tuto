create table car2 (
	car_uuid UUID NOT NULL PRIMARY KEY,
	make VARCHAR(50) NOT NULL,
	model VARCHAR(50) NOT NULL,
	price NUMERIC(19,2) NOT NULL
);


create table person2 (
	person_uuid UUID NOT NULL PRIMARY KEY,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
	gender VARCHAR(7) NOT NULL,
    email VARCHAR(100),
	date_of_birth DATE NOT NULL,
    country_of_birth VARCHAR(50) NOT NULL,
    car_uuid_foreign uuid REFERENCES car2(car_uuid)
);


insert into person2 (person_uuid, first_name, last_name, gender, email, date_of_birth, country_of_birth) values (uuid_generate_v4(), 'JC', 'Dansereau', 'Male', 'jc@jici.ca', '1990-11-03', 'Canada');
insert into person2 (person_uuid, first_name, last_name, gender, email, date_of_birth, country_of_birth) values (uuid_generate_v4(), 'Maggie', 'Crepe', 'DogCow', 'maggie@jici.ca', '2021-11-03', 'Canada');
insert into person2 (person_uuid, first_name, last_name, gender, email, date_of_birth, country_of_birth) values (uuid_generate_v4(),'Jess', 'Ouellet', 'Female', 'jess@jici.ca', '1990-07-02', 'Canada');

insert into car2 (car_uuid, make, model,price) values (uuid_generate_v4(), 'Tesla', 'Model 3', '56000.00');
insert into car2 (car_uuid, make, model,price) values (uuid_generate_v4(), 'Ford', 'F-150', '49000.50');